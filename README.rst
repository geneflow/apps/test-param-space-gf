Test Param Space GeneFlow App
=============================

Version: 0.1

This GeneFlow app prints a string to the screen and to a file.

Inputs
------

1. input: Dummy input file.

Parameters
----------

1. string: String to print. Default: 'hello'.
 
2. output: Output text file.

